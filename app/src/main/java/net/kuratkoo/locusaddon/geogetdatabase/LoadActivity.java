package net.kuratkoo.locusaddon.geogetdatabase;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import net.kuratkoo.locusaddon.geogetdatabase.exception.NoCachesFoundException;
import net.kuratkoo.locusaddon.geogetdatabase.exception.OutOfMemoryException;
import net.kuratkoo.locusaddon.geogetdatabase.util.Geoget;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import locus.api.android.ActionDisplay.ExtraAction;
import locus.api.android.ActionDisplayPoints;
import locus.api.android.ActionTools;
import locus.api.android.objects.PackWaypoints;
import locus.api.android.utils.LocusInfo;
import locus.api.android.utils.LocusUtils;
import locus.api.objects.extra.Location;
import locus.api.objects.extra.Waypoint;
import locus.api.objects.geocaching.GeocachingData;
import locus.api.objects.geocaching.GeocachingWaypoint;

/**
 * LoadActivity
 *
 * @author Radim -kuratkoo- Vaculik <kuratkoo@gmail.com>
 * @author Jakub Jerabek <jerabek.jakub@gmail.com>
 */
public class LoadActivity extends Activity implements DialogInterface.OnDismissListener {

	private static final String TAG = "LocusAddonGeogetDatabase|LoadActivity";
	private boolean importCaches;
	private ProgressDialog progress;
	private PackWaypoints packWpt;
	private File externalDir;
	private Waypoint point;
	private LoadAsyncTask loadAsyncTask;
	private SharedPreferences sp;

	public void onDismiss(DialogInterface arg0) {
		loadAsyncTask.cancel(true);
	}

	private class LoadAsyncTask extends AsyncTask<Waypoint, Integer, Exception> {

		private SQLiteDatabase db;

		@Override
		protected void onPreExecute() {
			progress.show();
			try {
				db = SQLiteDatabase.openDatabase(
						URLDecoder.decode(sp.getString("db", ""), "UTF-8"),
						null,
						SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			} catch (UnsupportedEncodingException uee) {
				Toast.makeText(LoadActivity.this, uee.getLocalizedMessage(), Toast.LENGTH_LONG).show();
			}
			importCaches = sp.getBoolean("import", true);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			progress.setMessage(getString(R.string.loading) + " " + values[0] + "/" + values[1] + " " + getString(R.string.geocaches));
		}

		protected Exception doInBackground(Waypoint... pointSet) {
			try {
				if (this.isCancelled()) {
					return null;
				}

				// user's position
				Waypoint pp = pointSet[0];

				Location curr = new Location(TAG);
				curr.setLatitude(pp.getLocation().getLatitude());
				curr.setLongitude(pp.getLocation().getLongitude());

				packWpt = new PackWaypoints("Geoget data");
				Float radius = Float.valueOf(sp.getString("radius", "1")) / 70;
				Float radiusWP = (float) (radius * Math.sqrt(2)) / 2;
				String[] cond = new String[]{
						String.valueOf(pp.getLocation().getLatitude() - radius),
						String.valueOf(pp.getLocation().getLatitude() + radius),
						String.valueOf(pp.getLocation().getLongitude() - radius),
						String.valueOf(pp.getLocation().getLongitude() + radius)
				};
				String[] cond2 = new String[]{
						String.valueOf(pp.getLocation().getLatitude() - radiusWP),
						String.valueOf(pp.getLocation().getLatitude() + radiusWP),
						String.valueOf(pp.getLocation().getLongitude() - radiusWP),
						String.valueOf(pp.getLocation().getLongitude() + radiusWP),
						String.valueOf(pp.getLocation().getLatitude() - radiusWP),
						String.valueOf(pp.getLocation().getLatitude() + radiusWP),
						String.valueOf(pp.getLocation().getLongitude() - radiusWP),
						String.valueOf(pp.getLocation().getLongitude() + radiusWP)
				};

				String sql = "SELECT geocache.x, geocache.y, geocache.id FROM geocache ";

				if (sp.getBoolean("solved", false)) {
					sql += " LEFT JOIN waypoint ON geocache.id = waypoint.id ";
				}

				sql += "WHERE cachestatus IN (0";
				String sqlWP = "SELECT waypoint.x, waypoint.y, waypoint.id " +
						"FROM waypoint LEFT JOIN geocache ON geocache.id = waypoint.id " +
						"WHERE cachestatus IN (0";

				// Disabled geocaches
				if (sp.getBoolean("disable", false)) {
					sql += ",1";
					sqlWP += ",1";
				}

				// Archived geocaches
				if (sp.getBoolean("archive", false)) {
					sql += ",2";
					sqlWP += ",2";
				}

				sql += ") ";
				sqlWP += ") ";

				// Filter solved caches
				if (sp.getBoolean("solved", false)) {
					sql += " AND wpttype=\"Final Location\" AND waypoint.x > 0 AND waypoint.y > 0 ";
					sqlWP += " AND wpttype=\"Final Location\" AND waypoint.x > 0 AND waypoint.y > 0 ";
				}

				// Found by user
				if (!sp.getBoolean("found", false)) {
					sql += " AND dtfound = 0 ";
					sqlWP += " AND dtfound = 0 ";
				}

				// Owned by user
				if (!sp.getBoolean("own", false)) {
					LocusInfo info = ActionTools.getLocusInfo(LoadActivity.this, LocusUtils.getActiveVersion(LoadActivity.this));
					String author = info.getGcOwnerName();
					if (author != null && author.length() > 0) {
						sql += " AND author != \"" + author + "\"";
						sqlWP += " AND author != \"" + author + "\"";
					}
				}

				// Filter cache type
				List<String> geocacheTypes = Geoget.geocacheTypesFromFilter(sp);

				if (!geocacheTypes.isEmpty() && geocacheTypes.size() != Geoget.countTypes) {
					String firstType = geocacheTypes.remove(0);
					sql += " AND cachetype IN (\"" + firstType + "\"";
					sqlWP += " AND cachetype IN (\"" + firstType + "\"";

					for (String geocacheType : geocacheTypes) {
						sql += ", \"" + geocacheType + "\"";
						sqlWP += ", \"" + geocacheType + "\"";
					}
					sql += ") ";
					sqlWP += ") ";
				}

				// Filter cache size
				List<String> geocacheSizes = Geoget.geocacheSizesFromFilter(sp);
				if (!geocacheSizes.isEmpty() && geocacheSizes.size() != Geoget.countSizes) {
					String firstSize = geocacheSizes.remove(0);
					sql += " AND cachesize IN (\"" + firstSize + "\"";
					sqlWP += " AND cachesize IN (\"" + firstSize + "\"";

					for (String geocacheSize : geocacheSizes) {
						sql += ", \"" + geocacheSize + "\"";
						sqlWP += ", \"" + geocacheSize + "\"";
					}
					sql += ") ";
					sqlWP += ") ";
				}

				// Filter terrain
				List<String> geocacheTerrains = Geoget.geocacheDiffTerrFromFilter(sp, "terr");
				if (!geocacheTerrains.isEmpty() && geocacheTerrains.size() != Geoget.countTerr) {
					String firstTerrain = geocacheTerrains.remove(0);
					sql += " AND terrain IN (\"" + firstTerrain + "\"";
					sqlWP += " AND terrain IN (\"" + firstTerrain + "\"";

					for (String geocacheTerrain : geocacheTerrains) {
						sql += ", \"" + geocacheTerrain + "\"";
						sqlWP += ", \"" + geocacheTerrain + "\"";
					}
					sql += ") ";
					sqlWP += ") ";
				}

				// Filter difficulty
				List<String> geocacheDifficulties = Geoget.geocacheDiffTerrFromFilter(sp, "diff");
				if (!geocacheDifficulties.isEmpty() && geocacheDifficulties.size() != Geoget.countDiff) {
					String firstDiff = geocacheDifficulties.remove(0);
					sql += " AND difficulty IN (\"" + firstDiff + "\"";
					sqlWP += " AND difficulty IN (\"" + firstDiff + "\"";

					for (String geocacheDiff : geocacheDifficulties) {
						sql += ", \"" + geocacheDiff + "\"";
						sqlWP += ", \"" + geocacheDiff + "\"";
					}
					sql += ") ";
					sqlWP += ") ";
				}

				sql += " AND CAST(geocache.x AS REAL) > ? AND CAST(geocache.x AS REAL) < ? AND CAST(geocache.y AS REAL) > ? AND CAST(geocache.y AS REAL) < ?";
				sqlWP += " AND (CAST(waypoint.x AS REAL) > ? AND CAST(waypoint.x AS REAL) < ? AND CAST(waypoint.y AS REAL) > ? AND CAST(waypoint.y AS REAL) < ?) " +
						"AND (CAST(geocache.x AS REAL) <= ? OR CAST(geocache.x AS REAL) >= ? OR CAST(geocache.y AS REAL) <= ? OR CAST(geocache.y AS REAL) >= ?)";

				Cursor c = db.rawQuery(sql, cond);
				Cursor cWP = db.rawQuery(sqlWP, cond2);

				/** Load GC codes **/
				List<Pair> gcCodes = new ArrayList<Pair>();
				radius = radius * 70 * 1000;
				// Caches
				while (c.moveToNext()) {
					Location loc = new Location(TAG);
					loc.setLatitude(c.getDouble(0));
					loc.setLongitude(c.getDouble(1));
					if (loc.distanceTo(curr) < radius) {
						gcCodes.add(new Pair(loc.distanceTo(curr), c.getString(2)));
					}
				}
				c.close();

				if (this.isCancelled()) {
					cWP.close();
					return null;
				}

				// Waypoints
				while (cWP.moveToNext()) {
					Location loc = new Location(TAG);
					loc.setLatitude(cWP.getDouble(0));
					loc.setLongitude(cWP.getDouble(1));
					if (loc.distanceTo(curr) < radius) {
						gcCodes.add(new Pair(loc.distanceTo(curr), cWP.getString(2)));
					}
				}
				cWP.close();

				if (this.isCancelled()) {
					return null;
				}

				// no caches found
				if (gcCodes.size() == 0) {
					return new NoCachesFoundException();
				}

				int limit = Integer.parseInt(sp.getString("limit", "0"));

				// Limit
				if (limit > 0 && gcCodes.size() > limit) {
					// sort caches
					Collections.sort(gcCodes, new Comparator<Pair>() {
						public int compare(Pair p1, Pair p2) {
							return p1.distance.compareTo(p2.distance);
						}
					});

					// cut off the rest
					gcCodes = gcCodes.subList(0, limit);
				}

				// columns
				String columns = " geocache.id, x, y, name, difficulty, terrain, cachesize, cachetype, cachestatus, dtfound, author, hint ";

				// GC codes
				int total = gcCodes.size();
				String codesCond = "(\"" + gcCodes.remove(0).gcCode + "\"";
				for (Pair pair : gcCodes) {
					codesCond += ",\"" + pair.gcCode + "\"";
				}
				codesCond += ")";

				String query = "SELECT " + columns + " FROM geocache INNER JOIN geolist ON geocache.id = geolist.id ";
				query += " WHERE geocache.id IN " + codesCond;
				Cursor caches = db.rawQuery(query, null);

				int count = 0;
				String queryTags = "SELECT geotagcategory.value AS key, geotagvalue.value FROM geotag " +
						"INNER JOIN geotagcategory ON geotagcategory.key = geotag.ptrkat " +
						"INNER JOIN geotagvalue ON geotagvalue.key = geotag.ptrvalue " +
						"WHERE geotagcategory.value IN (\"favorites\", \"PMO\") AND geotag.id = ?";
				String queryWPs = "SELECT x, y, name, wpttype, cmt, prefixid, comment, flag FROM waypoint WHERE id = ?";

				while (caches.moveToNext()) {
					if (this.isCancelled()) {
						caches.close();
						return null;
					}

					publishProgress(++count, total);

					Location loc = new Location(TAG);
					// geocache.id, x, y, name, difficulty, terrain, cachesize, cachetype, cachestatus, dtfound, author
					loc.setLatitude(caches.getDouble(1));
					loc.setLongitude(caches.getDouble(2));
					Waypoint wpt = new Waypoint(caches.getString(3), loc);

					GeocachingData gcData = new GeocachingData();
					gcData.setCacheID(caches.getString(0));
					gcData.setName(caches.getString(3));
					gcData.setDifficulty(caches.getFloat(4));
					gcData.setTerrain(caches.getFloat(5));
					gcData.setContainer(Geoget.convertCacheSize(caches.getString(6)));
					gcData.setType(Geoget.convertCacheType(caches.getString(7)));
					gcData.setAvailable(Geoget.isAvailable(caches.getInt(8)));
					gcData.setArchived(Geoget.isArchived(caches.getInt(8)));
					gcData.setFound(Geoget.isFound(caches.getInt(9)));
					gcData.setOwner(caches.getString(10));
					gcData.setPlacedBy(caches.getString(10));
					gcData.setLatOriginal(caches.getDouble(1));
					gcData.setLonOriginal(caches.getDouble(2));
                    gcData.setEncodedHints(caches.getString(11));

					/** Add PMO tag, favorite points and elevation **/
					Cursor tags = db.rawQuery(queryTags, new String[]{gcData.getCacheID()});
					while (tags.moveToNext()) {
						String key = tags.getString(0);
						String value = tags.getString(1);

						if (key.equals("PMO") && value.equals("X")) {
							gcData.setPremiumOnly(true);
						} else if (key.equals("favorites")) {
							try {
								gcData.setFavoritePoints(Integer.parseInt(value));
							} catch (NumberFormatException e) {
								// do nothing
							}
						}
					}
					tags.close();

					/** Add waypoints to Geocache **/
					Cursor wp = db.rawQuery(queryWPs, new String[]{gcData.getCacheID()});
					while (wp.moveToNext()) {
						GeocachingWaypoint pgdw = new GeocachingWaypoint();
						pgdw.setLat(wp.getDouble(0));
						pgdw.setLon(wp.getDouble(1));
						pgdw.setName(wp.getString(2));
						pgdw.setType(Geoget.convertWaypointType(wp.getString(3)));
						pgdw.setCode(wp.getString(5));
						String desc = wp.getString(4);

						String comment = wp.getString(6);
						if (comment != null && !comment.equals("")) {
							desc += " <hr><b>" + getString(R.string.wp_personal_note) + "</b> " + comment;
						}
						pgdw.setDesc(desc);

						gcData.waypoints.add(pgdw);
					}
					wp.close();

					/** Add geocaching data to waypoint **/
					wpt.gcData = gcData;

					/** Where to obtain more data **/
					wpt.setExtraOnDisplay(
							"net.kuratkoo.locusaddon.geogetdatabase",
							"net.kuratkoo.locusaddon.geogetdatabase.DetailActivity",
							"cacheId",
							gcData.getCacheID()
					);

					/** Add waypoint to list **/
					packWpt.addWaypoint(wpt);
				}
				caches.close();

				return null;
			} catch (OutOfMemoryError ofme) {
				return new OutOfMemoryException();
			} catch (Exception e) {
				return e;
			}
		}

		@Override
		protected void onPostExecute(Exception ex) {
			super.onPostExecute(ex);
			progress.dismiss();
			db.close();
			if (ex != null) {
				if (ex.getClass().getName().contains("NoCachesFoundException")) {
					Toast.makeText(LoadActivity.this, getString(R.string.no_caches_found), Toast.LENGTH_LONG).show();
					LoadActivity.this.finish();
				} else if (ex.getClass().getName().contains("OutOfMemoryException")) {
					AlertDialog.Builder ad = new AlertDialog.Builder(LoadActivity.this);
					ad.setIcon(android.R.drawable.ic_dialog_alert);
					ad.setTitle(R.string.error);
					ad.setMessage(R.string.out_of_memory);
					ad.setPositiveButton(android.R.string.ok, new OnClickListener() {
						public void onClick(DialogInterface di, int arg1) {
							di.dismiss();
							LoadActivity.this.finish();
						}
					});
					ad.show();
				} else {
					Toast.makeText(LoadActivity.this, getString(R.string.unable_to_load_geocaches) + " (" + ex.getClass() + ")", Toast.LENGTH_LONG).show();
					LoadActivity.this.finish();
				}
			} else {

				String filePath = externalDir.getAbsolutePath();
				if (!filePath.endsWith("/")) {
					filePath += "/";
				}
				filePath += "Android/data/net.kuratkoo.locusaddon.geogetdatabase/data.locus";

				ArrayList<PackWaypoints> data = new ArrayList<>();
				data.add(packWpt);

				try {
					boolean centerDisplay = sp.getBoolean("center", true);
					ActionDisplayPoints.sendPacksFile(
							LoadActivity.this,
							data,
							filePath,
							(importCaches ? ExtraAction.IMPORT : (centerDisplay ? ExtraAction.CENTER : ExtraAction.NONE)));
				} catch (OutOfMemoryError ofme) {
					AlertDialog.Builder ad = new AlertDialog.Builder(LoadActivity.this);
					ad.setIcon(android.R.drawable.ic_dialog_alert);
					ad.setTitle(R.string.error);
					ad.setMessage(R.string.out_of_memory);
					ad.setPositiveButton(android.R.string.ok, new OnClickListener() {
						public void onClick(DialogInterface di, int arg1) {
							di.dismiss();
							LoadActivity.this.finish();
						}
					});
					ad.show();
				} catch (Exception e) {
					Toast.makeText(LoadActivity.this, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
			}
		}


		@Override
		protected void onCancelled() {
			super.onCancelled();
			db.close();
			progress.dismiss();
			Toast.makeText(LoadActivity.this, getString(R.string.canceled), Toast.LENGTH_LONG).show();
			LoadActivity.this.finish();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sp = PreferenceManager.getDefaultSharedPreferences(LoadActivity.this);

		progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading_dots));
		progress.setIcon(android.R.drawable.ic_dialog_info);
		progress.setTitle(getString(R.string.loading));
		progress.setOnDismissListener(this);

		externalDir = Environment.getExternalStorageDirectory();
		if (externalDir == null || !(externalDir.exists())) {
			Toast.makeText(LoadActivity.this, getString(R.string.no_external_storage), Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		File fd;
		try {
			fd = new File(URLDecoder.decode(sp.getString("db", ""), "UTF-8"));
			if (!Geoget.isGeogetDatabase(fd)) {
				Toast.makeText(LoadActivity.this, getString(R.string.no_db_file), Toast.LENGTH_LONG).show();
				finish();
				return;
			}
		} catch (Exception ex) {
			Toast.makeText(LoadActivity.this, ex.getLocalizedMessage(), Toast.LENGTH_LONG).show();
		}

		Intent intent = getIntent();
		if (LocusUtils.isIntentMainFunction(intent)) {

			LocusUtils.handleIntentMainFunction(LoadActivity.this, intent, new LocusUtils.OnIntentMainFunction() {

                @Override
                public void onReceived(LocusUtils.LocusVersion lv, Location locGps, Location locMapCenter) {
                    // get map center
                    point = new Waypoint("Map center", locMapCenter);
                }

                @Override
				public void onFailed() {
					Toast.makeText(LoadActivity.this, "Wrong INTENT!", Toast.LENGTH_LONG).show();
				}
			});
		} else if (LocusUtils.isIntentPointTools(intent)) {
			try {
				point = LocusUtils.handleIntentPointTools(this, intent);
			} catch (Exception rvme) {
				Toast.makeText(LoadActivity.this, rvme.getLocalizedMessage(), Toast.LENGTH_LONG).show();
			}
		} else if (LocusUtils.isIntentSearchList(intent)) {

			LocusUtils.handleIntentSearchList(LoadActivity.this, intent, new LocusUtils.OnIntentMainFunction() {

                @Override
                public void onReceived(LocusUtils.LocusVersion lv, Location locGps, Location locMapCenter) {
                    // get map center
                    point = new Waypoint("Map center", locMapCenter);
                }

                @Override
				public void onFailed() {
					Toast.makeText(LoadActivity.this, "Wrong INTENT!", Toast.LENGTH_SHORT).show();
				}
			});
		}

		if (point != null) {
			loadAsyncTask = new LoadAsyncTask();
			loadAsyncTask.execute(point);
		} else {
			Toast.makeText(LoadActivity.this, R.string.unable_to_get_position, Toast.LENGTH_LONG).show();
			finish();
		}
	}

	private class Pair {

		private String gcCode;
		private Float distance;

		public Pair(Float f, String s) {
			this.distance = f;
			this.gcCode = s;
		}
	}
}
