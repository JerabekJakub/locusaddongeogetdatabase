package net.kuratkoo.locusaddon.geogetdatabase.exception;

/**
 * Custom exception
 *
 * @author Jakub Jerabek <jerabek.jakub@gmail.com>
 * @author Radim -kuratkoo- Vaculik <kuratkoo@gmail.com>
 */
public class OutOfMemoryException extends Exception {
}