package net.kuratkoo.locusaddon.geogetdatabase;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spanned;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.codechimp.apprater.AppRater;

import java.net.URLDecoder;

import locus.api.android.ActionTools;

/**
 * MainActivity
 *
 * @author Radim -kuratkoo- Vaculik <kuratkoo@gmail.com>
 * @author Jakub Jerabek <jerabek.jakub@gmail.com>
 */
public class MainActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {

	private static final String TAG = "LocusAddonGeogetDatabase|MainActivity";
	private Preference dbPick;
	private Preference attachPick;
	private EditTextPreference logsCount;
	private EditTextPreference radius;
	private EditTextPreference limit;
	private CheckBoxPreference own;
	private AdView mAdView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		addPreferencesFromResource(R.xml.prefs);
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

		own = (CheckBoxPreference) getPreferenceScreen().findPreference("own");

		dbPick = (Preference) getPreferenceScreen().findPreference("db_pick");
		dbPick.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			public boolean onPreferenceClick(Preference pref) {
				try {
					ActionTools.actionPickFile(MainActivity.this, 0, getText(R.string.pref_db_pick_title).toString(), new String[]{".db3"});
				} catch (ActivityNotFoundException anfe) {
					Toast.makeText(MainActivity.this, "Error: " + anfe.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
				return true;
			}
		});
		dbPick.setSummary(editPreferenceSummary(URLDecoder.decode(PreferenceManager.getDefaultSharedPreferences(this).getString("db", "")), getText(R.string.pref_db_sum)));

		attachPick = (Preference) getPreferenceScreen().findPreference("attach_pick");
		attachPick.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			public boolean onPreferenceClick(Preference pref) {
				try {
					ActionTools.actionPickDir(MainActivity.this, 1, getText(R.string.pref_db_pick_dir_title).toString());
				} catch (ActivityNotFoundException anfe) {
					Toast.makeText(MainActivity.this, "Error: " + anfe.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
				return true;
			}
		});
		attachPick.setSummary(editPreferenceSummary(PreferenceManager.getDefaultSharedPreferences(this).getString("attach", ""), getText(R.string.pref_attach_sum)));

		radius = (EditTextPreference) getPreferenceScreen().findPreference("radius");
		radius.setSummary(editPreferenceSummary(radius.getText() + " km", getText(R.string.pref_radius_sum)));

		logsCount = (EditTextPreference) getPreferenceScreen().findPreference("logs_count");
		logsCount.setSummary(editPreferenceSummary(logsCount.getText(), getText(R.string.pref_logs_sum)));

		limit = (EditTextPreference) getPreferenceScreen().findPreference("limit");
		if (limit.getText().equals("0")) {
			limit.setSummary(editPreferenceSummary(getString(R.string.pref_limit_nolimit), getText(R.string.pref_limit_sum)));
		} else {
			limit.setSummary(editPreferenceSummary(limit.getText(), getText(R.string.pref_limit_sum)));
		}

		if (!own.isEnabled()) {
			own.setSummary(Html.fromHtml(getString(R.string.pref_own_sum) + " <b>" + getString(R.string.pref_own_fill) + "</b>"));
		}

		mAdView = (AdView) this.findViewById(R.id.ad);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);

		AppRater.app_launched(this);
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals("db")) {
		}

		if (key.equals("attach")) {
		}

		if (key.equals("logs_count")) {
			String value = sharedPreferences.getString(key, "20");
			if (value.equals("") || !value.matches("[0-9]+")) {
				Toast.makeText(this, getString(R.string.pref_logs_error), Toast.LENGTH_LONG).show();
				value = "20";
				logsCount.setText(value);
			}
			logsCount.setSummary(editPreferenceSummary(value, getText(R.string.pref_logs_sum)));
		}

		if (key.equals("radius")) {
			String value = sharedPreferences.getString(key, "1");
			if (value.equals("") || !value.matches("[1-9][0-9]*|[0-9]\\.[0-9]")) {
				Toast.makeText(this, getString(R.string.pref_radius_error), Toast.LENGTH_LONG).show();
				value = "1";
				radius.setText(value);
				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString("radius", value);
				editor.commit();
			}
			radius.setSummary(editPreferenceSummary(value + " km", getText(R.string.pref_radius_sum)));
		}

		if (key.equals("limit")) {
			String value = sharedPreferences.getString(key, "0");
			if (value.equals("") || !value.matches("[0-9]+")) {
				Toast.makeText(this, getString(R.string.pref_limit_error), Toast.LENGTH_LONG).show();
				limit.setText("0");
				limit.setSummary(editPreferenceSummary(getString(R.string.pref_limit_nolimit), getText(R.string.pref_logs_sum)));
			} else if (value.equals("0")) {
				limit.setSummary(editPreferenceSummary(getString(R.string.pref_limit_nolimit), getText(R.string.pref_limit_sum)));
			} else {
				limit.setSummary(editPreferenceSummary(value, getText(R.string.pref_limit_sum)));
			}
		}
	}

	private Spanned editPreferenceSummary(String value, CharSequence summary) {
		if (!value.equals("")) {
			return Html.fromHtml("<font color=\"#FF8000\"><b>(" + value + ")</b></font> " + summary);
		} else {
			return Html.fromHtml(summary.toString());
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK && data != null) {
				String filename = data.getData().toString().replace("file://", "");
				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString("db", filename);
				editor.commit();
				dbPick.setSummary(editPreferenceSummary(filename, getText(R.string.pref_db_sum)));
			}
		} else if (requestCode == 1) {
			if (resultCode == RESULT_OK && data != null) {
				String path = data.getData().toString().replace("file://", "");
				if (!path.endsWith("/")) {
					path += "/";
				}
				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString("attach", path);
				editor.commit();
				attachPick.setSummary(editPreferenceSummary(path, getText(R.string.pref_attach_sum)));
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdView != null) {
			mAdView.resume();
		}
	}

	@Override
	protected void onPause() {
		if (mAdView != null) {
			mAdView.pause();
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (mAdView != null) {
			mAdView.destroy();
		}
		super.onDestroy();
	}
}
